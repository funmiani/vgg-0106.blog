const rendBlog = document.getElementById('rendBlog'); 
const commentRugged = document.getElementById('commentRugged');
const singlePost = document.querySelector('.innerStoryOne');
const postUrl = 'https://jsonplaceholder.typicode.com/posts'; // The Api link for post blog
const callComment = 'https://jsonplaceholder.typicode.com/comments?postId=1';   //The Api link for post comment


function postAny() {
    fetch(postUrl)
    .then(response => response.json())    
    .then(json =>{
(function () {
    document.getElementById("first").addEventListener("click", firstPage);
        document.getElementById("next").addEventListener("click", nextPage);
        document
          .getElementById("previous")
          .addEventListener("click", previousPage);
        document.getElementById("last").addEventListener("click", lastPage);

        var list = json
        var pageList = new Array();
        var currentPage = 1;
        var numberPerPage = 10;
        var numberOfPages = 0;
    
    function makeList() {
        numberOfPages = getNumberOfPages();
    }
        
    function getNumberOfPages() {
        return Math.ceil(list.length / numberPerPage);
    }
    
    function nextPage() {
        currentPage += 1;
        loadList();
    }
    
    function previousPage() {
        currentPage -= 1;
        loadList();
    }
    
    function firstPage() {
        currentPage = 1;
        loadList();
    }
    
    function lastPage() {
        currentPage = numberOfPages;
        loadList();
    }
    
    function loadList() {
        var begin = ((currentPage - 1) * numberPerPage);
        var end = begin + numberPerPage;
    
        pageList = list.slice(begin, end);
        drawList();
        check();
    }
        
    function drawList() {
        rendBlog.innerHTML ="";

        pageList.map(value=>{
            rendBlog.innerHTML += `
            <div class="innerHead" >
              <div class="innerHeadOne">
              <div class="innerContent">
                <div>
                    <h2>${value.title}</h2>
                    <p>Paul Bolton <a href="#">Design</a> April 9, 2019</p>
                </div>
                <div>
                    <img src="img/blog-img.jpg" alt="" srcset="">
                </div>
                <div>
                <p>${value.body}</p>
                  <button id="readMore" class=" btn btn-primary " onClick="postComment(${value.id})">Read more</button>
                </div> 
                </div>
                </div>
            </div>
             `
             
        })
    }
    
    function check() {
        document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
        document.getElementById("previous").disabled = currentPage == 1 ? true : false;
        document.getElementById("first").disabled = currentPage == 1 ? true : false;
        document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
    }
    
    function load() {
        makeList();
        loadList();
    }
        
    window.onload = load();
})();
        

        
 } ); 

}
postAny();


let queryParameters = location.search;
 
let sanitizedQP = queryParameters.substring(1); // Remove question mark(?) from url search
 
// console.log(sanitizedQP);
 
let sanitizedQPArray = sanitizedQP.split("&");
 
// console.log(sanitizedQPArray);
 
params = {};
console.log(params)
 
sanitizedQPArray.forEach(function(sanitizedQP) {
  paramsKeyValueArray = sanitizedQP.split("=");
  params[paramsKeyValueArray[0]] = paramsKeyValueArray[1];
});

renderPostAndComment(params.id)
console.log(params.id)

function renderPostAndComment(id) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then(response => response.json())    
    .then(json =>{
       
        singlePost.innerHTML+= `
        <div class="innerContentStory"><div><h2>${json.title}</h2><p>Paul Bolton <a href="#">Design</a> April 9, 2019</p></div><div><img src="img/blog-img.jpg" alt="" srcset=""></div><div><p>${json.body}
        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
        </p></div></div>`
    })
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}/comments/?postId=${id}`)
        .then(response => response.json())
        .then(json => {
            json.map(value => {
                commentRugged.innerHTML += `<div class="boxCommment"><h4 class="small-title">Comments</h4><div class="row"><div class="col-lg-12"><div class="comments-list"><div class="media"><a class="media-left" href="#"><img src="upload/author.jpg" alt="" class="rounded-circle"></a><div class="media-body"><h4 class="media-heading user_name">${value.name}</h4><p>${value.email}</p><p>${value.body}</p><a href="#" class="btn btn-primary btn-sm">Reply</a></div></div>`
    })
 } ); 

}


function postComment(id) {
    
    window.location.href =`BlogRead.html?id=${id}`;
    
};

// pagination for the blog

